/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <inops/utils/misc/date.hh>
#include <inops/utils/exception.hh>

#include <sstream>

#include <ctime>

namespace inops
{
    std::string date(const std::string & format)
    {
        char t[255];
        time_t tt(std::time(0));
        std::ostringstream s;

        if (0 == strftime(t, 255, format.c_str(), gmtime(&tt)))
            throw InternalError(INOPS_HERE, "strftime failed");

        s << t;

        return s.str();
    }
}
