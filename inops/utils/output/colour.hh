/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * Based upon src/output/colour.hh from Paludis, which is:
 *   Copyright (c) 2006, 2007 Ciaran McCreesh
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef INOPS_GUARD_INOPS_UTILS_OUTPUT_COLOUR_HH
#define INOPS_GUARD_INOPS_UTILS_OUTPUT_COLOUR_HH 1

#include <string>

#include <paludis/util/stringify.hh>

namespace inops
{
    enum Colour
    {
        cl_red         = 31,
        cl_green       = 32,
        cl_yellow      = 33,
        cl_blue        = 34,
        cl_pink        = 35,

        cl_bold_red    = cl_red + 100,
        cl_bold_green  = cl_green + 100,
        cl_bold_yellow = cl_yellow + 100,
        cl_bold_blue   = cl_blue + 100,
        cl_bold_pink   = cl_pink + 100,

        cl_notice      = cl_bold_green,
        cl_info        = cl_bold_blue,
        cl_error       = cl_bold_red
    };

    bool using_colours();
    void set_use_colours(bool value);
    void set_force_colours(bool value);

    template<typename T_>
    std::string colour(Colour c, const T_ & v)
    {
        if (! using_colours())
            return paludis::stringify(v);
        else
            return "\033[" + paludis::stringify(static_cast<unsigned>(c) / 100) + ";"
                + paludis::stringify(static_cast<unsigned>(c) % 100) + "m" + paludis::stringify(v)
                + "\033[0;0m";
    }
}

#endif
