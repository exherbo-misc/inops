/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * Based upon src/output/colour.cc from Paludis, which is:
 *   Copyright (c) 2006, 2007 Ciaran McCreesh
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <paludis/util/system.hh>
#include <unistd.h>

namespace inops
{
    static bool can_use_colours = true;

    static bool force_colours = false;

    bool using_colours()
    {
        if (! can_use_colours)
            return false;

        static bool result(
            force_colours ||
            ((1 == isatty(1)) &&
                (0 != paludis::getenv_with_default("TERM", "").compare(0, 4, "dumb")))
        );

        return result;
    }

    void set_use_colours(bool value)
    {
        can_use_colours = value;
    }

    void set_force_colours(bool value)
    {
        force_colours = value;
    }
}
