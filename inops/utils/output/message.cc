/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <iostream>

#include <inops/utils/exception.hh>
#include <inops/utils/output/colour.hh>
#include <inops/utils/output/message.hh>

namespace inops
{
    void message(MessageLevel ml, const std::string & m)
    {
        do
        {
            switch (ml)
            {
                case ml_notice:
                    std::cout << colour(cl_notice, ">>> ") << m << " ..." << std::endl;
                    continue;
                case ml_info:
                    std::cout << colour(cl_info, ">>> ") << m << " ..." << std::endl;
                    continue;
                case ml_error:
                    std::cout << colour(cl_error, ">>> ") << m << " ..." << std::endl;
                    continue;
            }

            throw InternalError(INOPS_HERE, "Bad value for MessageLevel");

        } while (false);
    }
}
