/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef INOPS_GUARD_INOPS_UTILS_EXCEPTION_HH
#define INOPS_GUARD_INOPS_UTILS_EXCEPTION_HH 1

#include <string>
#include <exception>

#include <paludis/util/stringify.hh>

#define INOPS_HERE (std::string(__PRETTY_FUNCTION__) + " at " + std::string(__FILE__) + ":" + paludis::stringify(__LINE__))

namespace inops
{
    class Exception :
        public std::exception
    {
        private:
            const std::string _message;
            mutable std::string _what_string;
            const exception & operator=(const Exception &);

        protected:
            Exception(const std::string & message) throw ();
            Exception(const Exception &);

        public:
            virtual ~Exception() throw ();
            const std::string & message() const throw ();
    };

    class InternalError :
        public Exception
    {
        public:
            InternalError(const std::string & location, const std::string & message) throw ();
    };
}

#endif
