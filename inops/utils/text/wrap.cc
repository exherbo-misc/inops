/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <inops/utils/text/wrap.hh>

namespace inops
{
    namespace wrapper_internals
    {
        std::string rmtws(const std::string & text)
        {
            std::string s(text);

            if (s.empty())
                return "";

            for (std::string::iterator t(s.end()), t_end(s.begin()); t != t_end && std::isspace(*--t);)
                ;

            if (! std::isspace(*t))
                ++t;

            s.erase(t, s.end());

            return s;
        }
    }

    std::string wrap(const std::string & s, int width, const std::string & left_margin)
    {
        std::string tmp;

        char current('\0');
        char last('\0');

        width = width - left_margin.length();
        std::string text(left_margin + s + " ");

        int i(0);

        std::istringstream read(text);
        std::ostringstream output;

        while (read.get(current))
        {
            if (++i == width)
            {
                wrapper_internals::rmtws(tmp);
                output << std::endl << left_margin << tmp.substr(1, tmp.length());
                i = tmp.length();
                tmp.clear()
            }
            else if (std::isspace(current) && ! std::isspace(last))
            {
                output << tmp;
                tmp.clear();
            }

            tmp += current;
            last = current;
        }

        return output.str();
    }
}
