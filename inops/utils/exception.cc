/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <inops/utils/exception.hh>

namespace inops
{
    Exception::Exception(const std::string & message) throw () :
        _message(message)
    {
    }

    Exception::Exception(const Exception & other) :
        std::exception(other),
        _message(other._message)
    {
    }

    Exception::~Exception() throw ()
    {
    }

    const std::string & Exception::message() const throw ()
    {
        return _message;
    }

    InternalError::InternalError(const std::string & location, const std::string & message) throw () :
        Exception("Internal error at " + location + ": " + message)
    {
    }
}
