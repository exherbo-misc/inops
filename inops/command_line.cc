/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <inops/command_line.hh>

#include <paludis/util/instantiation_policy-impl.hh>
#include <paludis/util/visitor-impl.hh>

template class paludis::InstantiationPolicy<inops::CommandLine, paludis::instantiation_method::SingletonTag>;

namespace inops
{
    CommandLine::CommandLine() :
        ArgsHandler(),

        action_args(this, "Actions",
                "Selects which basic action to perform. Exactly one action should "
                "be specified."),
        a_version(&action_args,   "version",      'V',  "Display program version", false),
        a_help(&action_args,      "help",         'h',  "Display program help", false),

        general_args(this, "General options",
                "Options which are relevant for most options."),
        a_log_level(&general_args, "log-level",  '\0'),

        colour_args(this, "Colour options",
                "Selects when colour will be used."),
        a_colour(&colour_args, "colour", '\0', "Select whether to use colour.",
                paludis::args::EnumArg::EnumArgOptions
                ("always",          "Always use colour")
                ("never",           "Never use colour")
                ("auto",            "Use colour whenever it can be used"), "auto"),
        a_color(&a_colour, "color"),

        a_repository_dir(&general_args, "repository-dir", 'D', "Where to find the repository (default: current directory)"),
        a_download_dir(&general_args, "download-dir", 'd', "Where to place downloaded files"),
        a_master_repository_dir(&general_args, "master-repository-dir", '\0', "Use the specified location for the master repository"),
        a_write_cache_dir(&general_args, "write-cache-dir", '\0', "Use a subdirectory named for the repository name under the specified directory for repository write cache"),
        a_use_repository_cache(&general_args, "use-repository-cache", '\0', "Use the repository's metadata cache", false),

        vcs_args(this, "Version Control System options",
                "Select which version control system actions to perform."),
        a_commit(&vcs_args, "commit", 'c', "Perform a commit", false),
        a_commit_message(&vcs_args, "message", 'm', "Specify a commit message"),
        a_no_update(&vcs_args, "no-update", '\0', "Do not update the repository prior to performing other tasks", false),

        changelog_args(this, "Changelog options",
                "Select which changelog actions to perform."),
        a_changelog(&changelog_args, "changelog",    'C', "Add the commit message to the changelog", false),
        a_changelog_style(&changelog_args, "changelog-style", '\0', "Select which style you want your changelog in",
                paludis::args::EnumArg::EnumArgOptions
                ("paludis",          "Use Paludis' Changelog format")
                ("gentoo",           "Use Gentoo's ChangeLog format"), "paludis"),
        a_fixes(&changelog_args, "fixes", '\0', "Add a reference to a fixed bug in the changelog"),
        a_reference(&changelog_args, "reference", '\0', "Add a reference to a bug in the changelog"),

        manifest_args(this, "Manifest options",
                "Select which manifest actions to perform."),
        a_manifest(&manifest_args, "manifest", 'M', "Generate Manifest data", false),

        gpg_args(this, "GnuPG options",
                "Select which GnuPG actions to perform."),
        a_sign_manifest(&gpg_args, "sign", 'S', "Sign Manifest", false),

        keyword_args(this, "Keyword actions",
                "Selects which keyword actions to perform."),
        a_package_name(&keyword_args, "package", 'p', "Select which package(s) to work with"),
        a_keyword(&keyword_args, "keyword", 'k', "Select which keyword(s) to change")
    {
        add_environment_variable("INOPS_OPTIONS", "Default command-line options.");
    }

    CommandLine::~CommandLine()
    {
    }

    std::string CommandLine::app_name() const
    {
        return "inops";
    }

    std::string CommandLine::app_synopsis() const
    {
        return "A commit tool for package repositories";
    }

    std::string CommandLine::app_description() const
    {
        return "inops is a commit client for package repositories.";
    }
}
