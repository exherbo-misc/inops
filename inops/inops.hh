/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef INOPS_GUARD_INOPS_INOPS_HH
#define INOPS_GUARD_INOPS_INOPS_HH 1

#include <config.h>

#include <inops/command_line.hh>

#include <inops/utils/exception.hh>
#include <inops/utils/output/colour.hh>
#include <inops/utils/output/message.hh>
#include <inops/utils/text/banner.hh>
#include <inops/utils/text/uppercase.hh>
#include <inops/utils/text/capitalize.hh>
#include <inops/utils/misc/date.hh>

#include <inops/tasks/task.hh>
#include <inops/tasks/task_list.hh>

#include <inops/vcs/git.hh>
#include <inops/vcs/novcs.hh>
#include <inops/vcs/svn.hh>
#include <inops/vcs/vcs.hh>
#include <inops/vcs/vcs_file.hh>

namespace inops
{
    struct DoVersion
    {
    };
}

#endif
