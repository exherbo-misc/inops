/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <iostream>

#include <inops/inops.hh>

#include <paludis/about.hh>
#include <paludis/args/do_help.hh>
#include <paludis/util/log.hh>
#include <paludis/util/join.hh>
#include <paludis/util/fs_entry.hh>
#include <paludis/util/map.hh>
#include <paludis/environments/no_config/no_config_environment.hh>

#include <cstdlib>

using std::cout;
using std::cerr;
using std::endl;

using namespace paludis;
using namespace inops;

namespace inops
{
    FSEntry
    get_location()
    {
        Context context("When determining tree location:");

        if (CommandLine::get_instance()->a_repository_dir.specified())
            return FSEntry(CommandLine::get_instance()->a_repository_dir.argument());

        if ((FSEntry::cwd() / "profiles").is_directory())
            return FSEntry::cwd();
        if ((FSEntry::cwd().dirname() / "profiles").is_directory())
            return FSEntry::cwd().dirname();
        if ((FSEntry::cwd().dirname().dirname() / "profiles").is_directory())
            return FSEntry::cwd().dirname().dirname();

        throw ConfigurationError("Cannot find tree location (try specifying --repository-dir)");
    }
}

int main(int argc, char* argv[])
{
    Context context("In program " + join(argv, argv + argc, " ") + ":");

    try
    {
        CommandLine::get_instance()->run(argc, argv, "inops", "INOPS_OPTIONS", "INOPS_CMDLINE");

        if (argc < 2)
            throw args::DoHelp("At least one argument has to be specified");

        if (CommandLine::get_instance()->a_colour.specified())
        {
            if (CommandLine::get_instance()->a_colour.argument() == "always")
            {
                set_force_colours(true);
                set_use_colours(true);
            }
            else if (CommandLine::get_instance()->a_colour.argument() == "never")
                set_use_colours(false);
            else if (CommandLine::get_instance()->a_colour.argument() == "auto")
                set_use_colours(using_colours());
            else
                throw args::DoHelp("bad value for --colour");
        }

        if (CommandLine::get_instance()->a_help.specified())
            throw args::DoHelp();

        if (CommandLine::get_instance()->a_version.specified())
            throw DoVersion();

        if (CommandLine::get_instance()->a_log_level.specified())
            Log::get_instance()->set_log_level(CommandLine::get_instance()->a_log_level.option());
        else
            Log::get_instance()->set_log_level(ll_qa);

        if (! CommandLine::get_instance()->a_write_cache_dir.specified())
            CommandLine::get_instance()->a_write_cache_dir.set_argument("/var/empty");

        if (! CommandLine::get_instance()->a_master_repository_dir.specified())
            CommandLine::get_instance()->a_master_repository_dir.set_argument("/var/empty");

        tr1::shared_ptr<Map<std::string, std::string> > keys(new Map<std::string, std::string>);
        keys->insert("distdir", CommandLine::get_instance()->a_download_dir.argument());

        tr1::shared_ptr<NoConfigEnvironment> env(new NoConfigEnvironment(no_config_environment::Params::create()
                .repository_dir(get_location())
                .write_cache(CommandLine::get_instance()->a_write_cache_dir.argument())
                .accept_unstable(true)
                .repository_type(no_config_environment::ncer_ebuild)
                .master_repository_dir(CommandLine::get_instance()->a_master_repository_dir.argument())
                .disable_metadata_cache(! CommandLine::get_instance()->a_use_repository_cache.specified())
                .extra_params(keys)
                ));
    }
    catch (const paludis::args::ArgsError & e)
    {
        cerr << "Usage error: " << e.message() << endl;
        cerr << "Try " << argv[0] << " --help" << endl;

        return EXIT_FAILURE;
    }
    catch (const paludis::args::DoHelp & h)
    {
        if (h.message.empty())
        {
            cout << "Usage: " << argv[0] << " [options]" << endl;
            cout << endl;
            cout << *CommandLine::get_instance();

            return EXIT_SUCCESS;
        }
        else
        {
            cerr << "Usage error: " << h.message << endl;
            cerr << "Try " << argv[0] << " --help" << endl;

            return EXIT_FAILURE;
        }
    }
    catch (const DoVersion &)
    {
        cout << "Inops, version " << VERSION << endl;
        cout << "Built against " << PALUDIS_PACKAGE << " " << PALUDIS_VERSION_MAJOR << "." << PALUDIS_VERSION_MINOR << "." << PALUDIS_VERSION_MICRO << endl;
        cout << endl << endl;
        cout << "Inops comes with ABSOLUTELY NO WARRANTY. Inops is free software, and you" << endl;
        cout << "are welcome to redistribute it under the terms of the GNU General Public" << endl;
        cout << "License, version 2." << endl;

        return EXIT_SUCCESS;
    }
    catch (const inops::Exception & e)
    {
        message(ml_error, "Error: " + e.message() + " (" + e.what() + ")");

        return EXIT_FAILURE;
    }
    catch (const paludis::Exception & e)
    {
        cout << endl;
        cerr << "Unhandled exception:" << endl
             << "  * " << e.backtrace("\n  * ")
             << e.message() << " (" << e.what() << ")" << endl;

        return EXIT_FAILURE;
    }
    catch (const std::exception & e)
    {
        cout << endl;
        cerr << "Unhandled exception:" << endl
             << "  * " << e.what() << endl;

        return EXIT_FAILURE;
    }
    catch (...)
    {
        cout << endl;
        cerr << "Unhandled exception:" << endl
             << "  *  Unknown exception type. Ouch..." << endl;

        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
