/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <inops/changelog/paludis.hh>

#include <set>

namespace paludis
{
    template<>
    struct Implementation<inops::PaludisChangelog>
    {
        std::set<std::string> reference_bugs;
        std::set<std::string> fixed_bugs;
    };
}

namespace inops
{
    PaludisChangelog::PaludisChangelog() :
        paludis::PrivateImplementationPattern<PaludisChangelog>(new paludis::Implementation<PaludisChangelog>)
    {
    }

    PaludisChangelog::~PaludisChangelog()
    {
    }

    void PaludisChangelog::add_changeLog_entry(const std::string & message)
    {
        std::string changelog_header(banner("ChangeLog for " + capitalize(paludis::FSEntry::cwd().basename())));
        std::string tmp_message(wrap("* " + changelog_list_of_changed_files() + ": " + message, 75, "\t"));

        std::ostringstream changelog_entry;

        changelog_entry << date("%Y-%m-%d")
                        << " "
                        << "FIXME: REALNAME"
                        << " <" << "EMAIL" << ">"
                        << std::endl << std::endl
                        << tmp_message;

        if (! _imp->reference_bugs.empty())
        {
            changelog_entry << std::endl;

            for (std::set<std::string>::iterator t(_imp->reference_bugs.begin()), t_end(_imp->reference_bugs.end()); t != t_end; ++t)
                changelog_entry << std::endl << "\t+ See: " << *t;
        }

        if (! _imp->fixed_bugs.empty())
        {
            changelog_entry << std::endl;

            for (std::set<std::string>::iterator t(_imp->fixed_bugs.begin()), t_end(_imp->fixed_bugs.end()); t != t_end; ++t)
                changelog_entry << std::endl << "\t+ Fixes: " << *t;
        }

        paludis::FSEntry ChangeLog("ChangeLog");

        if (! ChangeLog.exists())
        {
            std::ofstream write_changelog("ChangeLog");

            if (write_changelog)
                write_changelog << changelog_header << std::endl << std::endl;
            else
                throw PaludisChangelogError("Unable to create new ChangeLog");
        }

        std::list<std::string> changelog_lines;

        {
            std::ifstream read_changelog("ChangeLog");

            if (read_changelog)
            {
                std::string line;

                while (std::getline(read_changelog, line))
                    changelog_lines.push_back(line);
            }
        }

        {
            std::ofstream changelog_final("ChangeLog");

            if (changelog_final)
            {
                for (std::list<std::string>::iterator t(changelog_lines.begin()), t_end(changelog_lines.end()); t != t_end; ++t)
                    changelog_final << *t << std::endl;
            }
            else
                throw PaludisChangelogError("Unable to write ChangeLog");
        }

        for (std::list<std::string>::iterator t(changelog_lines.begin()), t_end(changelog_lines.end()); t != t_end; ++t)
        {
            if (paludis::stringify(*t).empty())
            {
                lines.insert(t, "\n" + changelog_entry.str());
                break;
            }
        }
    }

    void PaludisChangelog::add_reference_bug(const std::string & bug_id);
    {
        _imp->reference_bugs.insert(bug_id);
    }

    void PaludisChangelog::add_fixed_bug(const std::string & bug_id)
    {
        _imp->fixed_bugs.insert(bug_id);
    }
}
