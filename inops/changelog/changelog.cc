/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <inops/changelog/changelog.hh>
#include <inops/changelog/paludis.hh>

namespace inops
{
    ChangeLog::~ChangeLog()
    {
    }

    paludis::tr1::shared_ptr<ChangeLog> create_changelog_object(const std::string & style)
    {
        if (style == "paludis")
            return paludis::tr1::shared_ptr<ChangeLog>(new PaludisChangeLog);
//        else if (style == "gentoo")
//            return paludis::tr1::shared_ptr<ChangeLog>(new GentooChangeLog);
        else
            return paludis::tr1::shared_ptr<ChangeLog>(new PaludisChangeLog);
    }

    std::string changelog_list_of_files(const paludis::tr1::shared_ptr<VCS> & vcs)
    {
        std::string result;

        for (VCSFileList::Iterator t(vcs->begin()), t_end(vcs-end()); t != t_end; ++t)
        {
            std::string file(paludis::stringify(t->get_file()));

            if (file == ".")
                continue;

            do
            {
                switch (t->get_file_state())
                {
                    case vcs_fs_added:
                        result += "+" + file;
                        continue;

                    case vcs_fs_removed:
                        result += "-" + file;
                        continue;

                    case vcs_fs_modified:
                        result += file;
                        continue;

                    case vcs_fs_untracked:
                        continue;

                    case vcs_fs_conflict:
                        continue;
                }

                throw InternalError(INOPS_HERE, "Bad value for VCSFileState");

            } while (false);

            if (t != t_end)
                result += ", ";
        }

        return result;
    }
}
