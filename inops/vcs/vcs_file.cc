/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <inops/vcs/vcs_file.hh>
#include <paludis/util/wrapped_forward_iterator-impl.hh>

#include <list>

template class paludis::WrappedForwardIterator<inops::VCSFileList::IteratorTag, paludis::tr1::shared_ptr<inops::VCSFile> >;

namespace paludis
{
    template<>
    struct Implementation<inops::VCSFile>
    {
        const FSEntry file;
        const inops::VCSFileState file_state;

        Implementation(const paludis::FSEntry & f, inops::VCSFileState fs) :
            file(f),
            file_state(fs)
        {
        }
    };

    template<>
    struct Implementation<inops::VCSFileList>
    {
        std::list<paludis::tr1::shared_ptr<inops::VCSFile> > file_list;
    };
}

namespace inops
{
    VCSFile::VCSFile(const paludis::FSEntry & file, VCSFileState file_state) :
        paludis::PrivateImplementationPattern<VCSFile>(new paludis::Implementation<VCSFile>(file, file_state))
    {
    }

    VCSFile::~VCSFile()
    {
    }

    paludis::FSEntry VCSFile::get_file() const
    {
        return _imp->file;
    }

    VCSFileState VCSFile::get_file_state() const
    {
        return _imp->file_state;
    }

    VCSFileList::VCSFileList() :
        paludis::PrivateImplementationPattern<VCSFileList>(new paludis::Implementation<VCSFileList>)
    {
    }

    VCSFileList::~VCSFileList()
    {
    }

    void VCSFileList::add_entry(const paludis::tr1::shared_ptr<VCSFile> & file)
    {
        _imp->file_list.push_back(file);
    }

    VCSFileList::Iterator
    VCSFileList::begin()
    {
        return Iterator(_imp->file_list.begin());
    }

    VCSFileList::Iterator
    VCSFileList::end()
    {
        return Iterator(_imp->file_list.end());
    }
}
