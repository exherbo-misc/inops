/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <inops/vcs/vcs.hh>

#include <paludis/util/fd_holder.hh>
#include <paludis/util/system.hh>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <inops/vcs/svn.hh>
#include <inops/vcs/git.hh>
#include <inops/vcs/novcs.hh>

namespace inops
{
    VCS::~VCS()
    {
    }

    bool git_check()
    {
        paludis::FDHolder dev_null(::open("/dev/null", O_WRONLY));
        paludis::set_run_command_stdout_fds(dev_null, -1);
        paludis::set_run_command_stderr_fds(dev_null, -1);

        return paludis::run_command("git rev-parse --git-dir") == 0;
    }

    paludis::tr1::shared_ptr<VCS> create_vcs_object(const paludis::FSEntry & dir)
    {
        if ((dir / ".svn/").exists())
            return paludis::tr1::shared_ptr<VCS>(new SVN);
        else if (git_check())
            return paludis::tr1::shared_ptr<VCS>(new Git);
        else
            return paludis::tr1::shared_ptr<VCS>(new NoVCS);
    }
}
