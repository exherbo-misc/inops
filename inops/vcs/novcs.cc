/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <inops/vcs/novcs.hh>
#include <inops/utils/output/message.hh>

#include <string>

namespace inops
{
    NoVCS::~NoVCS()
    {
    }

    void NoVCS::commit(const std::string &)
    {
        message(ml_info, "No VCS detected, skipping");
    }

    void NoVCS::update()
    {
        message(ml_info, "No VCS detected, skipping");
    }

    void NoVCS::add_file(const paludis::FSEntry &)
    {
        message(ml_info, "No VCS detected, skipping");
    }

    std::string NoVCS::get_name()
    {
        return "novcs";
    }

    paludis::tr1::shared_ptr<VCSFileList> NoVCS::get_files()
    {
        paludis::tr1::shared_ptr<VCSFileList> file_list(new VCSFileList);

        return file_list;
    }
}
