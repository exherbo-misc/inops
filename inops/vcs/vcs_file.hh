/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef INOPS_GUARD_INOPS_VCS_VCS_FILE_HH
#define INOPS_GUARD_INOPS_VCS_VCS_FILE_HH 1

#include <paludis/util/fs_entry.hh>
#include <paludis/util/private_implementation_pattern.hh>
#include <paludis/util/tr1_memory.hh>
#include <paludis/util/wrapped_forward_iterator.hh>

namespace inops
{
    enum VCSFileState
    {
        vcs_fs_added,
        vcs_fs_removed,
        vcs_fs_modified,
        vcs_fs_untracked,
        vcs_fs_conflict
    };

    class VCSFile :
        private paludis::PrivateImplementationPattern<VCSFile>
    {
        public:
            VCSFile(const paludis::FSEntry & file, VCSFileState file_state);
            ~VCSFile();

            paludis::FSEntry get_file() const;
            VCSFileState get_file_state() const;
    };

    class VCSFileList :
        private paludis::PrivateImplementationPattern<VCSFileList>
    {
        public:
            VCSFileList();
            ~VCSFileList();

            void add_entry(const paludis::tr1::shared_ptr<VCSFile> & file);

            struct IteratorTag;
            typedef paludis::WrappedForwardIterator<IteratorTag, paludis::tr1::shared_ptr<VCSFile> > Iterator;

            Iterator begin();
            Iterator end();
    };
}

#endif
