/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <inops/vcs/svn.hh>

#include <paludis/util/fd_holder.hh>
#include <paludis/util/system.hh>
#include <paludis/util/stringify.hh>
#include <paludis/util/tr1_memory.hh>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

namespace inops
{
    SVNError::SVNError(const std::string & message) throw () :
        Exception(message)
    {
    }

    SVN::~SVN()
    {
    }

    void SVN::commit(const std::string & message)
    {
        std::stringstream output;
        paludis::Command commit_command("svn commit -m \"" + message + "\"");

        paludis::FDHolder dev_null(::open("/dev/null", O_WRONLY));
        paludis::set_run_command_stdout_fds(dev_null, -1);
        paludis::set_run_command_stderr_fds(dev_null, -1);

        if (paludis::run_command(commit_command.with_captured_stdout_stream(&output)))
            throw SVNError("Unable to perform commit with svn: " + output.str());
    }

    void SVN::update()
    {
        std::stringstream output;
        paludis::Command update_command("svn update");

        paludis::FDHolder dev_null(::open("/dev/null", O_WRONLY));
        paludis::set_run_command_stdout_fds(dev_null, -1);
        paludis::set_run_command_stderr_fds(dev_null, -1);

        if (paludis::run_command(update_command.with_captured_stdout_stream(&output)))
            throw SVNError("Unable to update our repository with svn: " + output.str());
    }

    void SVN::add_file(const paludis::FSEntry & file)
    {
        std::stringstream output;
        paludis::Command add_command("svn add \"" + paludis::stringify(file) + "\"");

        paludis::FDHolder dev_null(::open("/dev/null", O_WRONLY));
        paludis::set_run_command_stdout_fds(dev_null, -1);
        paludis::set_run_command_stderr_fds(dev_null, -1);

        if (paludis::run_command(add_command.with_captured_stdout_stream(&output)))
            throw SVNError("Unable to add file \"" + paludis::stringify(file) + "\": " + output.str());

    }

    std::string SVN::get_name()
    {
        return "subversion";
    }

    paludis::tr1::shared_ptr<VCSFileList> SVN::get_files()
    {
        paludis::tr1::shared_ptr<VCSFileList> file_list(new VCSFileList);
        std::stringstream output;
        std::string line;
        paludis::Command status_command("svn status");

        paludis::FDHolder dev_null(::open("/dev/null", O_WRONLY));
        paludis::set_run_command_stdout_fds(dev_null, -1);
        paludis::set_run_command_stderr_fds(dev_null, -1);

        if (paludis::run_command(status_command.with_captured_stdout_stream(&output)))
            throw SVNError("Unable to run svn status: " + output.str());

        while (std::getline(output, line))
        {
            VCSFileState file_state(vcs_fs_untracked);
            paludis::FSEntry file_entry(line.substr(7, line.length()));
            char svn_file_status_token(line.at(0));

            if (svn_file_status_token == 'D')
                file_state = vcs_fs_removed;
            else if (svn_file_status_token == 'A')
                file_state = vcs_fs_added;
            else if (svn_file_status_token == 'M' || svn_file_status_token == 'R')
                file_state = vcs_fs_modified;

            file_list->add_entry(paludis::tr1::shared_ptr<VCSFile>(new VCSFile(file_entry, file_state)));
        }

        return file_list;
    }
}
