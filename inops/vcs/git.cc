/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <inops/vcs/git.hh>

#include <paludis/util/fd_holder.hh>
#include <paludis/util/system.hh>
#include <paludis/util/stringify.hh>
#include <paludis/util/tr1_memory.hh>
#include <paludis/util/strip.hh>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

namespace inops
{
    GitError::GitError(const std::string & message) throw () :
        Exception(message)
    {
    }

    Git::Git()
    {
        std::stringstream output;
        paludis::Command cmd("git rev-parse --git-dir");
        std::string line;

        paludis::FDHolder dev_null(::open("/dev/null", O_WRONLY));
        paludis::set_run_command_stdout_fds(dev_null, -1);
        paludis::set_run_command_stderr_fds(dev_null, -1);

        if (paludis::run_command(cmd.with_captured_stdout_stream(&output)))
            throw GitError("Unable to find git dir: " + output.str());

        std::getline(output, line);
        line.erase(line.length() - 4);
        git_dir.reset(new paludis::FSEntry(line));
    }

    Git::~Git()
    {
    }

    void Git::commit(const std::string & message)
    {
        std::stringstream output;
        paludis::Command commit_command("git commit -m \"" + message + "\"");

        paludis::FDHolder dev_null(::open("/dev/null", O_WRONLY));
        paludis::set_run_command_stdout_fds(dev_null, -1);
        paludis::set_run_command_stderr_fds(dev_null, -1);

        if (paludis::run_command(commit_command.with_captured_stdout_stream(&output)))
            throw GitError("Unable to perform commit with git: " + output.str());
    }

    void Git::update()
    {
        std::stringstream output;
        paludis::Command update_command("git pull");

        paludis::FDHolder dev_null(::open("/dev/null", O_WRONLY));
        paludis::set_run_command_stdout_fds(dev_null, -1);
        paludis::set_run_command_stderr_fds(dev_null, -1);

        if (paludis::run_command(update_command.with_captured_stdout_stream(&output)))
            throw GitError("Unable to perform update with git: " + output.str());
    }

    void Git::add_file(const paludis::FSEntry & file)
    {
        std::stringstream output;
        paludis::Command add_command("git add \"" + paludis::stringify(file) + "\"");

        paludis::FDHolder dev_null(::open("/dev/null", O_WRONLY));
        paludis::set_run_command_stdout_fds(dev_null, -1);
        paludis::set_run_command_stderr_fds(dev_null, -1);

        if (paludis::run_command(add_command.with_captured_stdout_stream(&output)))
            throw GitError("Unable to add file \"" + paludis::stringify(file) + "\" with git: " + output.str());
    }

    std::string Git::get_name()
    {
        return "git";
    }

    paludis::tr1::shared_ptr<VCSFileList> Git::get_files()
    {
        paludis::tr1::shared_ptr<VCSFileList> file_list(new VCSFileList);
        std::stringstream output;
        paludis::Command file_command("git diff --name-status HEAD -- .");
        std::string line;
        std::string::size_type end_cwd(paludis::stringify(paludis::FSEntry::cwd()).length());

        paludis::FDHolder dev_null(::open("/dev/null", O_WRONLY));
        paludis::set_run_command_stdout_fds(dev_null, -1);
        paludis::set_run_command_stderr_fds(dev_null, -1);

        if (paludis::run_command(file_command.with_captured_stdout_stream(&output)))
            throw GitError("Failed to find VCS files with git: " + output.str());

        while (std::getline(output, line))
        {
            std::string file_path(paludis::strip_leading_string(line.substr(1), std::string(" ")));
            file_path = (paludis::stringify(*git_dir) + file_path).erase(0, end_cwd);
            paludis::FSEntry file_entry(file_path.substr(1));
            VCSFileState file_state(vcs_fs_untracked);
            char git_state(line.at(0));

            if (git_state == 'A')
                file_state = vcs_fs_added;
            else if (git_state == 'M')
                file_state = vcs_fs_modified;
            else if (git_state == 'D')
                file_state = vcs_fs_removed;

            file_list->add_entry(paludis::tr1::shared_ptr<VCSFile>(new VCSFile(file_entry, file_state)));
        }

        return file_list;
    }
}
