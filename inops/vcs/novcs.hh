/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef INOPS_GUARD_INOPS_VCS_NOVCS_HH
#define INOPS_GUARD_INOPS_VCS_NOVCS_HH 1

#include <inops/vcs/vcs.hh>


namespace inops
{
    class NoVCS :
        public VCS
    {
        public:
            virtual ~NoVCS();
            virtual void commit(const std::string &);
            virtual void update();
            virtual void add_file(const paludis::FSEntry &);
            virtual std::string get_name();
            virtual paludis::tr1::shared_ptr<VCSFileList> get_files();
    };
}

#endif
