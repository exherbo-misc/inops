/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef INOPS_GUARD_INOPS_TASKS_TASK_LIST_HH
#define INOPS_GUARD_INOPS_TASKS_TASK_LIST_HH 1

#include <inops/tasks/task.hh>

#include <paludis/util/private_implementation_pattern.hh>
#include <paludis/util/wrapped_forward_iterator.hh>
#include <paludis/util/tr1_memory.hh>

namespace inops
{
    class TaskList :
        private paludis::PrivateImplementationPattern<TaskList>
    {
        public:
            TaskList();
            ~TaskList();

            void add(const paludis::tr1::shared_ptr<Task> & task);

            struct IteratorTag;
            typedef paludis::WrappedForwardIterator<IteratorTag, paludis::tr1::shared_ptr<Task> > Iterator;

            Iterator begin();
            Iterator end();
    };
}

#endif
