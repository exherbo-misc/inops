/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <inops/tasks/task_list.hh>

#include <paludis/util/wrapped_forward_iterator-impl.hh>

#include <list>

template class paludis::WrappedForwardIterator<inops::TaskList::IteratorTag, paludis::tr1::shared_ptr<inops::Task> >;

namespace paludis
{
    template<>
    struct Implementation<inops::TaskList>
    {
        std::list<paludis::tr1::shared_ptr<inops::Task> > task_list;
    };
}

namespace inops
{
    TaskList::TaskList() :
        paludis::PrivateImplementationPattern<TaskList>(new paludis::Implementation<TaskList>)
    {
    }

    TaskList::~TaskList()
    {
    }

    TaskList::Iterator
    TaskList::begin()
    {
        return Iterator(_imp->task_list.begin());
    }

    TaskList::Iterator
    TaskList::end()
    {
        return Iterator(_imp->task_list.end());
    }

    void TaskList::add(const paludis::tr1::shared_ptr<Task> & task)
    {
        _imp->task_list.push_back(task);
    }
}
