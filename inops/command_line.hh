/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2008 Alexander Færøy
 *
 * This file is part of the Inops commit client. Inops is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Inops is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef INOPS_GUARD_INOPS_COMMAND_LINE_HH
#define INOPS_GUARD_INOPS_COMMAND_LINE_HH 1

#include <paludis/args/args.hh>
#include <paludis/args/log_level_arg.hh>
#include <paludis/util/instantiation_policy.hh>

namespace inops
{
    class CommandLine :
        public paludis::args::ArgsHandler,
        public paludis::InstantiationPolicy<CommandLine, paludis::instantiation_method::SingletonTag>
    {
        friend class paludis::InstantiationPolicy<CommandLine, paludis::instantiation_method::SingletonTag>;

        private:
            CommandLine();
            ~CommandLine();

        public:
            ///\name Program information
            ///\{

            virtual std::string app_name() const;
            virtual std::string app_synopsis() const;
            virtual std::string app_description() const;

            ///\}

            ///\name Action arguments
            ///\{

            /// Action arguments.
            paludis::args::ArgsGroup action_args;

            /// --version
            paludis::args::SwitchArg a_version;

            /// --help
            paludis::args::SwitchArg a_help;

            ///\}

            ///\name General arguments
            ///\{

            /// General arguments
            paludis::args::ArgsGroup general_args;

            /// --log-level
            paludis::args::LogLevelArg a_log_level;

            /// Colour arguments
            paludis::args::ArgsGroup colour_args;

            /// --colour
            paludis::args::EnumArg a_colour;

            /// --color
            paludis::args::AliasArg a_color;

            /// --repository-dir
            paludis::args::StringArg a_repository_dir;

            /// --download-dir
            paludis::args::StringArg a_download_dir;

            /// --master-repository-dir
            paludis::args::StringArg a_master_repository_dir;

            /// --write-cache-dir
            paludis::args::StringArg a_write_cache_dir;

            /// --use-repository-cache
            paludis::args::SwitchArg a_use_repository_cache;

            ///\}

            ///\name VCS arguments
            ///\{

            /// VCS arguments
            paludis::args::ArgsGroup vcs_args;

            /// --commit
            paludis::args::SwitchArg a_commit;

            /// --message
            paludis::args::StringArg a_commit_message;

            /// --no-update
            paludis::args::SwitchArg a_no_update;

            ///\}

            ///\name Changelog arguments
            ///\{

            /// Changelog arguments
            paludis::args::ArgsGroup changelog_args;

            /// --changelog
            paludis::args::SwitchArg a_changelog;

            /// --changelog-style
            paludis::args::EnumArg a_changelog_style;

            /// --fixes
            paludis::args::StringArg a_fixes;

            /// --reference
            paludis::args::StringArg a_reference;

            ///\}

            ///\name Manifest arguments
            ///\{

            /// Manifest arguments
            paludis::args::ArgsGroup manifest_args;

            /// --manifest
            paludis::args::SwitchArg a_manifest;

            ///\}

            ///\name GPG arguments
            ///\{

            /// GPG arguments
            paludis::args::ArgsGroup gpg_args;

            /// --sign-manifest
            paludis::args::SwitchArg a_sign_manifest;

            ///\}

            ///\name Keyword arguments
            ///\{

            /// Keyword arguments
            paludis::args::ArgsGroup keyword_args;

            /// --package
            paludis::args::StringArg a_package_name;

            /// --keyword
            paludis::args::StringArg a_keyword;

            ///\}
    };
}

#endif
